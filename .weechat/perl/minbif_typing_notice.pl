#
# Copyright (C) 2014 nemunaire <nemunaire@nemunai.re>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# For minbif - displays when someone is typing a message to you, and notice them when you do.
#
# History:
# 2014-03-05, nemunaire<nemunaire@nemunai.re>:
#     version 0.1: initial release

use strict;
my $SCRIPT_NAME = "minbif_typing_notice";
my $VERSION = "0.1";

weechat::register($SCRIPT_NAME, "Nemunaire <nemunaire\@nemunai.re>", $VERSION,
                  "GPL3", "For minbif - displays when someone is typing a message to you, and notice them when you do.", "", "");
init_config();

my %h_typing;
my %h_sending;

weechat::bar_item_new("typing_notice", "draw_typing", "");
weechat::hook_modifier("irc_in_privmsg", "modifier_ctcp", "");
weechat::hook_signal("input_text_changed", "input_changed", "");

sub draw_typing
{
    my ($osefa, $osefb, $osefc) = @_;

    my $buffer = weechat::current_buffer();

    return "✎" if exists $h_typing{$buffer};
    return "";
}

sub input_changed
{
    my ($data, $signal, $type_data) = @_;

    my $buffer = weechat::current_buffer();
    my $buffer_name = weechat::buffer_get_string($buffer, "name");

    my $server_name = weechat::config_get_plugin("minbif_server");

    my $nick;
    if ($buffer_name =~ /^\Q$server_name\E\.(.*)/)
    {
	$nick = $1;
	return weechat::WEECHAT_RC_OK if $nick eq "request";
    }
    else {
	return weechat::WEECHAT_RC_OK;
    }

    my $buffer_text = weechat::buffer_get_string($buffer, "input");
    if ($buffer_text eq "" or $buffer_text =~ /^\//)
    {
	if (exists $h_sending{$buffer})
	{
	    weechat::command($buffer, "/mute -all ctcp $nick TYPING 0");
	    weechat::unhook($h_sending{$buffer});
	    delete $h_sending{$buffer};
	}
	return weechat::WEECHAT_RC_OK;
    }

    return weechat::WEECHAT_RC_OK if exists $h_sending{$buffer};

    weechat::command($buffer, "/mute -all ctcp $nick TYPING 1");
    $h_sending{$buffer} = weechat::hook_timer(5000, 0, 1, "sending_timeout", $buffer);

    return weechat::WEECHAT_RC_OK;
}

sub sending_timeout
{
    my ($buffer, $n) = @_;
    my $buffer_name = weechat::buffer_get_string($buffer, "name");
    my $server_name = weechat::config_get_plugin("minbif_server");

    if (exists $h_sending{$buffer} && $buffer_name =~ /^\Q$server_name\E\.(.*)/)
    {
	weechat::command($buffer, "/mute -all ctcp $1 TYPING 0");
        weechat::unhook($h_sending{$buffer});
	delete $h_sending{$buffer};
    }
    return weechat::WEECHAT_RC_OK;
}

sub typing_timeout
{
    my ($buffer, $n) = @_;

    if (exists $h_typing{$buffer})
    {
        weechat::unhook($h_typing{$buffer});
	delete $h_typing{$buffer};
    }
    weechat::bar_item_update("typing_notice")
}

sub modifier_ctcp
{
    my ($data, $modifier, $modifier_data, $string) = @_;

    if ($string =~ /:([^!]*)!([^\s]*)\sPRIVMSG\s([^\s]*)\s:\01TYPING\s([0-9])\01/)
    {
        my $buffer = weechat::buffer_search("irc", $modifier_data . "." . $1);

	weechat::unhook($h_typing{$buffer}) if exists $h_typing{$buffer};

        if ($4 == "1") {
	    $h_typing{$buffer} = weechat::hook_timer(42000, 0, 1, "typing_timeout", $buffer)
	}
        elsif ($4 == "0") {
	    delete $h_typing{$buffer} if (exists $h_typing{$buffer});
	}

	weechat::bar_item_update("typing_notice");
        return ""
    }
    return $string
}

sub init_config
{
    my $version = weechat::info_get("version_number", "") || 0;
    if (!weechat::config_is_set_plugin("minbif_server"))
    {
	weechat::config_set_plugin("minbif_server", "minbif")
    }
}
