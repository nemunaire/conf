#!/bin/sh

die() {
    echo "$1" >&2
    exit 1
}

in_list() {
    for i in $1
    do
	[ "$i" == "$2" ] && return 0
    done
    return 1
}

cd $(dirname "$0")

[ -n "${DESTDIR}" ] || DESTDIR=~

if [ "$1" == "install" ] || [ "$1" == "link" ] || [ "$1" == "links" ]
then
    REMOVE=1
    BIN="ln -L -s"
elif [ "$1" == "update" ] || [ "$1" == "pull" ]
then
    git stash &&
    git pull --recurse-submodules=yes --rebase `git remote | head -n 1` &&
    git stash pop
    exit $?
elif [ "$1" == "remove" ] || [ "$1" == "uninstall" ]
then
    REMOVE=0
    BIN="rm"
elif [ "$1" == "copy" ]
then
    REMOVE=1
    BIN="cp -L -r"
else
    echo -e "\e[32;01mNemunaire's configuration \e[0;33m("`git branch --no-color | grep '*' | cut -d " " -f 2-`")\e[0m"
    echo -e "    \e[01mAuthor:\e[0m\t\t"`git log -1 --format="%aN <%aE>"`
    echo -e "    \e[01mOriginal source:\e[0m\thttps://git.nemunai.re/?p=conf.git"
    echo
    echo -e "\e[01mUsage:\e[0m"
    echo -e "    $0 \e[36;01minstall\e[0m"
    echo -e "        Create symbolic links from this directory to your home"
    echo -e "    $0 \e[36;01mremove\e[0m"
    echo -e "        Remove symbolic links from your home"
    echo -e "    $0 \e[36;01mupdate\e[0m"
    echo -e "        Pull latest commits from the server at" `git remote -v | head -n 1 | cut -f 2`
    echo -e "    $0 \e[36;01mcopy\e[0m"
    echo -e "        Copy configuration files instead of making links"
    exit 0
fi

git submodule update --init --recursive || die "Unable to update submodules"

IGNORE_FILES=".
              ..
              .git
              .gitmodules
              install.sh"

TMPERR=`mktemp`
doins() {
    echo -ne "Installing ${1##./} ...\t"

    # Alignment
    [ ${#1} -lt 11 ] && echo -en "\t"; [ ${#1} -lt 19 ] && echo -en "\t"

    if [ -L "$2" ]
    then
	echo -e "\e[36mAlready installed\e[0m"
    elif [ -d "$2" ] && [ -d "$1" ]
    then
	echo -e "\e[33mExistant directory\e[0m"
	DESTDIR="${DESTDIR}/$1" install_dir "$1"
    elif [ -e "$2" ]
    then
	echo -e "\e[35mAlready exists\e[0m"
    else
	if $BIN "$(pwd)/$1" "$2" >&2 2> "$TMPERR"
	then
	    echo -e "\e[32mdone\e[0m"
	else
	    echo -e "\e[31;01mfail\e[0m"
	fi
	cat "$TMPERR"
    fi
}

install_dir() {
    for f in `ls -a "$1"`
    do
	if [ "$1/$f" == "./.mozilla" ]
	then
	    if [ -d "${DESTDIR}/.mozilla/firefox" ]
	    then
		find ${DESTDIR}/.mozilla/firefox -mindepth 1 -maxdepth 1 -type d |
		while read dest
		do
		    mozilla_case "$1/$f" "$dest"
		done
	    else
		echo -e "Installing .mozilla/firefox ...\t\t\e[34mSkipped\e[0m"
	    fi
	elif [ "$1/$f" == "./.config" ]
	then
            DESTDIR="${DESTDIR}/.config" install_dir .config
	elif ! in_list "$IGNORE_FILES" "$f"
	then
            if [ "$REMOVE" -eq 0 ]; then
		echo -ne "Removing $1/$f ...\t"
		if [ -L "${DESTDIR}/$1/$f" ]; then
                    $BIN "${DESTDIR}/$1/$f" >&2 2> "$TMPERR"
	            if [ $? == 0 ]; then echo -e "\e[32mdone\e[0m"; else echo -e "\e[31;01mfail\e[0m"; fi
	            cat "$TMPERR"
		else
                    echo -e "Not installed"
		fi
            else
		doins "$1/$f" "${DESTDIR}/$f"
            fi
	fi
    done
}

mozilla_case() {
    echo -e "Installing .mozilla/firefox/${2##*/} ...\t\e[33mExisting profile\e[0m"
    DESTDIR="${2}" install_dir .mozilla/firefox
}

# Common installation
install_dir .

rm "${TMPERR}"
