# If not running interactively, don't do anything
[ -z "$PS1" ] && return

export CC=gcc
#export CFLAGS="-Wall -W -Werror -pedantic -ansi"
export EDITOR="emacs"
# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoreboth
export HISTSIZE=16384
export LANG=fr_FR.UTF-8
export MALLOC_CHECK_=3
export NNTPSERVER='news.epita.fr'
if command most 2> /dev/null
then
    export PAGER="most"
elif command less 2> /dev/null
then
    export PAGER="less"
fi

export LESS_TERMCAP_mb=$(printf "\e[1;37m")
export LESS_TERMCAP_md=$(printf "\e[1;31m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf "\e[1;47;30m")
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[1;32m")

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# append to the history file, don't overwrite it
shopt -s histappend
# Interdire l'écrasement de fichier avec >
set -o noclobber

# Prepend cd to directory names automatically
shopt -s autocd
# Correct spelling errors during tab-completion
shopt -s dirspell
# Correct spelling errors in arguments supplied to cd
shopt -s cdspell

# Perform file completion in a case insensitive fashion
bind "set completion-ignore-case on"

# Treat hyphens and underscores as equivalent
bind "set completion-map-case on"

PS1=""
MD5CMD=`(which md5sum > /dev/null && echo "md5sum") ||
(which md5 > /dev/null && echo "md5") || echo "cat"`

case `echo $(hostname) | $MD5CMD | sed -E 's/^.(.).*$/\1/'` in
    "1"|"a"|"b")
        HOST_COLOR=33 ;;
    "2"|"9"|"c")
        HOST_COLOR=32 ;;
    "3"|"9"|"d")
        HOST_COLOR=35 ;;
    "4"|"7"|"e")
        HOST_COLOR=34 ;;
    "5"|"6"|"f")
        HOST_COLOR=36 ;;
    *)
        HOST_COLOR=37 ;;
esac

if [ "$USER" == "root" ]; then
    HOST_COLOR=31
else
    PS1=$PS1"\u@"
fi
PS1=$PS1'\[\e[$HOST_COLOR;01m\]\h\[\e[00m\]:\[\e[01;34m\]\W\[\e[00m\]'
PS1=$PS1'\[`if [ $? -eq 0 ]; then echo -ne "\033[0;32m"; else echo -ne "\033[0;31m"; fi`\]'
PS1=$PS1'\$\[\033[0m\] '

#SSH agent
if [ -z "$SSH_AUTH_SOCK" ]; then # Keeps SSH forwarded agent
    if [ ! -f /tmp/ssh-agent.$USER ]; then
	ssh-agent > /tmp/ssh-agent.$USER
	chmod 400 /tmp/ssh-agent.$USER
    fi
    source /tmp/ssh-agent.$USER > /dev/null
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if [ -f ~/.mybashrc ]; then
    . ~/.mybashrc
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
