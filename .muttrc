set spoolfile = imaps://seshat.pomail.fr/INBOX
set imap_user = nemunaire
set imap_passive="no"
set imap_check_subscribed="yes"
set imap_list_subscribed="yes"

set smtp_url  = smtps://$imap_user@thot.pomail.fr
set smtp_pass = $imap_pass

set mask="!^\\.[^.]"
folder-hook . set record="+.Sent"
set postponed="+.Drafts"

# Set the desired default "from" address for both header From and envelope-from
set reverse_name
folder-hook . set from="nemunaire@nemunai.re"
alternates "nemunaire@pomail.fr|nemu@pomail.fr|pom@pomail.fr|pierreolivier@pomail.fr|pierre-olivier@pomail.fr|pierre-olivier@happydns.org|nemunaire@happydns.org|pierre-olivier.mercier@happydns.org|pierre-olivier@happydomain.org|nemunaire@happydomain.org|pierre-olivier.mercier@happydomain.org|nemunaire@epita.fr|mercie_d@epita.fr|pierreolivier.mercier@epita.fr|pierre-olivier.mercier@epita.fr|parava@pomail.fr"
set realname="Pierre-Olivier Mercier"
set hostname="nemunai.re"
set envelope_from=yes
set use_domain=no

set mail_check_stats
#set beep_new

# character set on sent messages
set send_charset="us-ascii:utf-8"
# if there is no character set given on incoming messages, it is probably windows
set assumed_charset="iso-8859-1"

# make sure Vim knows Mutt is a mail client and that a UTF-8 encoded message will be composed
set editor="vim -c 'set tw=72 syntax=mail ft=mail enc=utf-8'"

# we want to see some MIME types inline, see below this code listing for explanation
auto_view text/html
alternative_order text/plain text text/enriched text/html

# make default search pattern to search in To, Cc and Subject
set simple_search="~f %s | ~C %s | ~s %s"

# threading preferences, sort by threads
set sort=threads
set strict_threads=yes
set sort_aux=date-received
#set sort_browser=date

# do not show all headers, just a few
#ignore "Authentication-Results:"
#ignore "DomainKey-Signature:"
#ignore "DKIM-Signature:"
hdr_order Date From To Cc Bcc User-Agent

ignore *
unignore Disposition-Notification-To
unignore Authentication-Results
unignore from: date subject to cc mail-followup-to
unignore x-mailing-list: posted-to:
unignore x-mailer: x-url reply-to
unignore user-agent:
unignore Saved-In:

# use headercache (make sure this is a directory for better performance!)
set header_cache = ~/.mutt/cache
#set header_cache_pagesize = 32768
#set maildir_header_cache_verify = no

# set up the sidebar
set sidebar_divider_char='│'
set sidebar_format = '%B%?F? [%F]?%* %?N?%N?'
set sidebar_folder_indent
set sidebar_visible
set sidebar_sort_method=path
set sidebar_short_path

# make the progress updates not that expensive, this will update the bar every 300ms
set read_inc = 1
set time_inc = 300

set dsn_return=hdrs
set dsn_notify="failure,delay"

set auto_tag

# includes only the first attachment of the message you are replying
set include_onlyfirst
#set mime_forward                          # Forward message as MIME attachments.

# For better looks
#set arrow_cursor
#set folder_format="%t%N %-30.30f %8s"
#set index_format="%4C %Z %{%b %d} %-31.31F %N (%4c) %s"
set menu_scroll  # just scroll one line instead of full page
set nomarkers # don't put '+' at the beginning of wrapped lines
set pager_index_lines = 7 # how large is the index window?
set pager_stop

set date_format="%A %d %b %Y à %H:%M:%S (%Z)"
set attribution="Le %d, %n a écrit :"

# Aliases
set alias_file= ~/.mutt/aliases
set sort_alias= alias
set reverse_alias=yes
source $alias_file

# =====================================================================
# Custom bindings
# =====================================================================

macro index,pager c "<change-folder>?<toggle-mailboxes>" "open a different folder"
macro index,pager C "<copy-message>?<toggle-mailboxes>" "copy a message to a mailbox"
macro index,pager M "<save-message>?<toggle-mailboxes>" "move a message to a mailbox"
macro pager D "<pipe-message>~/scripts/dmarc-parser.pl<enter>" "parse attached DMARC report"
macro pager \ei "<pipe-message>~/.mutt/readical.py<enter>" "parse attached iCal"
macro pager T "<pipe-message>~/scripts/tlsrpt-report-display.pl<enter>" "parse attached SMTP-TLS report"
#macro pager V "<pipe-message>~/workspace/peret/check.py --students-list /home/nemunaire/workspace/peret/SRS2023.csv --real-send --review-before-send --skip-max-submission --skip-public-key --submissions '/home/nemunaire/workspace/virli/rendus/project3'<enter>" "rerun peret checker script for VIRLI"
macro pager V "<pipe-message>~/workspace/peret/check.py --students-list /home/nemunaire/workspace/peret/SRS2023.csv --real-send --review-before-send --skip-max-submission --skip-public-key --submissions '/home/nemunaire/workspace/adlin/rendus/TP2'<enter>" "rerun peret checker script for ADLIN"
#macro pager V "<pipe-message>~/workspace/peret/check.py --students-list /home/nemunaire/workspace/peret/SRS2023.csv --real-send --review-before-send --skip-max-submission --sign<enter>" "rerun peret checker script for VIRLI"
macro pager \eV "<pipe-message>~/workspace/peret/check.py --students-list /home/nemunaire/workspace/peret/SRS2023.csv --real-send --review-before-send --sign<enter>" "rerun peret checker script for signcheck"

macro compose A "<attach-message>?" "attach message(s) to this message"

bind index ' ' tag-entry
macro index,pager \eL '<change-folder>imaps://seshat.pomail.fr/Lost<enter>'
#macro index,pager \eV '<change-folder>=.Epita.SRS.virli/<enter>'
bind index,pager \CP sidebar-page-up
#bind index,pager \CI sidebar-page-down
bind index,pager \CO sidebar-open
bind index,pager i   sidebar-next
macro index,pager I '<sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next><sidebar-next>'
bind index,pager o   sidebar-open
bind index,pager p   sidebar-prev
macro index,pager P '<sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev><sidebar-prev>'
bind index,pager B   bounce-message

# b toggles sidebar visibility
bind index,pager b sidebar-toggle-visible

# Remap bounce-message function to "B"
bind index B bounce-message
bind index,pager \e<tab> next-unread-mailbox


# =====================================================================
# PGP
# =====================================================================

set crypt_use_gpgme = yes
# you can set this to hide gpg's verification output and only rely on Mutt's status flag
#set crypt_display_signature = no
# enable signing of emails by default
folder-hook . set pgp_autosign
#set pgp_replysign
#set pgp_replyencrypt
#set pgp_replysignencrypted
#set pgp_use_gpg_agent = yes
folder-hook . set pgp_sign_as = 0x842807A84573CC96

# Keep fcc's clear of signatues and encryption.
set fcc_clear


# =====================================================================
# Colors
# =====================================================================

color normal	  white		default
color message	  white		default
color signature   magenta	default
color hdrdefault  cyan		default
color attachment  yellow	default
color status	  brightblack	cyan
color search      brightwhite   magenta
color bold        brightyellow  default
color tilde	  brightblue	default              # ``~'' used to pad blank lines
color tree	  brightwhite	default              # thread tree in the index menu
color body	  brightblue	default "(git|ssh|ftp|http)s?://[^ ]+"       # point out URLs
color body	  brightblue	default [-a-z_0-9.]+@[-a-z_0-9.]+ # e-mail addresses

# Index

#color index color248 default "~A"

# Unread messages in yellow
color index cyan default "~t nemunaire@nemunai.re"
color index brightyellow default "~N|~O"
color index brightcyan default "~t nemunaire@nemunai.re (~N|~O)"

# Messages already replied to in green
color index green default "~Q"

# Deleted messages in a dark color
color index color237 default "~D"

# Flagged messages in red
color index brightred default "~F"
color index magenta default "~T"

# Compose
color compose header yellow default
color compose security_none red default
color compose security_encrypt magenta default
color compose security_sign cyan default
color compose security_both green default

# Headers

# Color information about the sender
color header brightcyan default "From: "
color header brightcyan default "Subject: "
color header brightcyan default "Date: "
color header brightyellow default "Saved-In: "
color header magenta default "Disposition-Notification-To: "

# DKIM
color header blue default "Authentication-Results:.*spf=none"
color header blue default "Authentication-Results:.*spf=neutral"
color header blue default "Authentication-Results:.*dkim=neutral"
color header red default "Authentication-Results:.*spf=fail"
color header red default "Authentication-Results:.*dkim=fail"
# DMARC
color header yellow default "Authentication-Results:.*dmarc=quarantine"
color header red default "Authentication-Results:.*dmarc=reject"


# Body

# Color the first levels of quoted text
color quoted green default
color quoted1 cyan default
color quoted2 green default
color quoted3 cyan default

# Sidebar colors
color sidebar_divider cyan default
color sidebar_new yellow default
color sidebar_flagged brightred default

# GnuPG output ########################################################
# Make if very obvious there is a bad signature!
color body default red "^gpg: BAD signature .*$"
color body default red "^gpg: MAUVAISE signature .*$"

# Tag a good signature
color body brightgreen default "^gpg: Good signature .*$"
color body brightgreen default "^gpg: Bonne signature .*$"
color body green default "^gpg: Good signature .*unknown]"
color body green default "^gpg: Bonne signature .*inconnu]"

# Inform the signature can't be verified
color body brightred default "^gpg: Can't check signature: public key not found"
color body brightred default "^gpg: Impossible de v.*"
color body brightred default "^gpg: WARNING: .*"
color body brightred default "^gpg: Attention: .*"
color body red default "^gpg: .*ette clef n'est pas certifiée avec une signature de confiance."
color body red default "^gpg: .*ien n'indique que la signature appartient à son propriétaire."
color body brightred default "^gpg: some signal caught ... exiting"

#Messages
color error brightwhite red                 # error messages
color message brightblack green             # status messages


# =====================================================================
# Hooks
# =====================================================================

source ~/.mutt/folder-hooks
folder-hook ".Appart" set from="pierre-olivier@pomail.fr"
folder-hook ".Appart" unset pgp_autosign
folder-hook ".Appart" unset pgp_sign_as

folder-hook ".Business.happyDomain" set from="nemunaire@happydomain.org"
folder-hook ".Business.happyDomain" unset pgp_autosign
folder-hook ".Business.happyDomain" unset pgp_sign_as

folder-hook ".Epita" set from="pierre-olivier.mercier@epita.fr"
folder-hook ".Epita" unset pgp_autosign
folder-hook ".Epita" unset pgp_sign_as

folder-hook ".Epita.SRS" set from="nemunaire@nemunai.re"
folder-hook ".Epita.SRS" set pgp_autosign
folder-hook ".Epita.SRS" set pgp_sign_as
