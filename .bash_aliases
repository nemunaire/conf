alias ed='ed -p "ed> "'
alias emacs='emacs -nw'
alias ne='emacs'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias j='jobs'
alias l='ls -F'
alias ls='ls --color=auto'
alias ll='ls -lh'
alias la='ls -a'
alias lla='ls -lha'

alias findh='find ./'
alias findf='findh -type f'
alias findd='findh -type d'

alias c='var=$(cal -m); echo "${var/$(date +%-d)/$(echo -e "\033[1;31m$(date +%-d)\033[0m")}"'
alias clean="rm *~ *.toc *.out *.aux \\#*\\#"
alias mirror="wget --mirror --no-parent"
alias ssh-add='ssh-add -t 10000'
alias rdesktop='rdesktop -k us-intl'
