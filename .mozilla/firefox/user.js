/*
  Sources:
    - https://github.com/pyllyukko/user.js
    - https://github.com/amq/firefox-debloat
*/

/* HTML5 / APIs / DOM */

user_pref("geo.enabled", false);
user_pref("media.peerconnection.enabled", false);
user_pref("media.navigator.enabled", false);
user_pref("dom.battery.enabled", false);
user_pref("dom.telephony.enabled", false);
user_pref("beacon.enabled", false);
user_pref("dom.event.clipboardevents.enabled", false);
user_pref("dom.enable_performance", false);
user_pref("media.webspeech.recognition.enable", false);
user_pref("media.getusermedia.screensharing.enabled", false);
user_pref("device.sensors.enabled", false);
user_pref("browser.send_pings", false);
user_pref("browser.send_pings.require_same_host", true);

/* misc */

user_pref("browser.search.defaultenginename", "DuckDcukGo");
user_pref("clipboard.autocopy", false);
user_pref("browser.fixup.alternate.enabled", false);
user_pref("network.proxy.socks_remote_dns", true);
user_pref("network.proxy.type", 0);
user_pref("security.mixed_content.block_active_content", true);
user_pref("security.mixed_content.block_display_content", true);
user_pref("javascript.options.methodjit.chrome", false);
user_pref("javascript.options.methodjit.content", false);
user_pref("javascript.options.asmjs", false);
user_pref("gfx.font_rendering.opentype_svg.enabled", false);

/* extensions / plugins  */

user_pref("plugin.state.flash", 0);
user_pref("plugins.click_to_play", true);
user_pref("extensions.update.enabled", true);
user_pref("extensions.blocklist.enabled", true);

/* firefox features / components */

user_pref("toolkit.telemetry.enabled", false);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("browser.polaris.enabled", true);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.healthreport.service.enabled", false);
user_pref("browser.newtabpage.enhanced", false);
user_pref("browser.newtab.preload", false);
user_pref("browser.newtabpage.directory.ping", "");
user_pref("browser.selfsupport.url", "");
user_pref("loop.enabled", false);
user_pref("browser.safebrowsing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.pocket.enabled", false);
user_pref("media.eme.enabled", false);
user_pref("media.gmp-eme-adobe.enabled", false);
user_pref("browser.search.suggest.enabled", false);

/* automatic connections */

user_pref("browser.search.geoip.url", "");
user_pref("network.predictor.enabled", false);
user_pref("browser.casting.enabled", false);
user_pref("media.gmp-gmpopenh264.enabled", false);
user_pref("media.gmp-manager.url", "");
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("browser.aboutHomeSnippets.updateUrl", "");
user_pref("browser.search.update", false);

/* HTTP */

user_pref("network.negotiate-auth.allow-insecure-ntlm-v1", false);
//user_pref("network.negotiate-auth.allow-insecure-ntlm-v1-https", false);
user_pref("security.csp.experimentalEnabled", true);
user_pref("security.csp.enable", true);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("network.http.sendRefererHeader", 1);
user_pref("network.http.referer.spoofSource", true);
user_pref("network.http.sendSecureXSiteReferrer", false);
user_pref("network.cookie.cookieBehavior", 1);

/* UI related */

user_pref("dom.event.contextmenu.enabled", false);
user_pref("plugins.update.notifyUser", true);
user_pref("security.warn_entering_weak", true);
user_pref("security.ssl.warn_missing_rfc5746", 1);
user_pref("security.ask_for_password", 0);
user_pref("browser.xul.error_pages.expert_bad_cert", 2);

/* TLS / HTTPS / OCSP related stuff */

user_pref("network.stricttransportsecurity.preloadlist", true);
user_pref("network.http.spdy.enabled", true);
user_pref("network.http.spdy.enabled.v3", true);
user_pref("network.http.spdy.enabled.v3-1", true);
user_pref("security.OCSP.enabled", true);
user_pref("security.ssl.enable_ocsp_stapling", true);
user_pref("security.OCSP.require", true);
user_pref("security.enable_tls_session_tickets", false);

user_pref("security.enable_ssl3", false);
user_pref("security.cert_pinning.enforcement_level", 2);
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
user_pref("security.ssl.errorReporting.automatic", false);

/* CIPHERS */

user_pref("security.ssl3.rsa_null_sha", false);
user_pref("security.ssl3.rsa_null_md5", false);
user_pref("security.ssl3.ecdhe_rsa_null_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_null_sha", false);
user_pref("security.ssl3.ecdh_rsa_null_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_null_sha", false);

user_pref("security.ssl3.rsa_seed_sha", false);

// 40 bits
user_pref("security.ssl3.rsa_rc4_40_md5", false);
user_pref("security.ssl3.rsa_rc2_40_md5", false);

// 56 bits
user_pref("security.ssl3.rsa_1024_rc4_56_sha", false);

// 128 bits
user_pref("security.ssl3.rsa_camellia_128_sha", false);
//user_pref("security.ssl3.rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_sha", false);
user_pref("security.ssl3.ecdh_rsa_aes_128_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_aes_128_sha", false);
user_pref("security.ssl3.dhe_rsa_camellia_128_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_128_sha", false);

// RC4 (CVE-2013-2566)
user_pref("security.ssl3.ecdh_ecdsa_rc4_128_sha", false);
user_pref("security.ssl3.ecdh_rsa_rc4_128_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_rc4_128_sha", false);
user_pref("security.ssl3.ecdhe_rsa_rc4_128_sha", false);
user_pref("security.ssl3.rsa_rc4_128_md5", false);
user_pref("security.ssl3.rsa_rc4_128_sha", false);

/*
* 3DES -> false because effective key size < 128
*
* https://en.wikipedia.org/wiki/3des#Security
* http://en.citizendium.org/wiki/Meet-in-the-middle_attack
*
* see also: http://www-archive.mozilla.org/projects/security/pki/nss/ssl/fips-ssl-ciphersuites.html
*/
user_pref("security.ssl3.dhe_dss_des_ede3_sha", false);
user_pref("security.ssl3.dhe_rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdh_rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_ecdsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdhe_rsa_des_ede3_sha", false);
user_pref("security.ssl3.rsa_des_ede3_sha", false);
user_pref("security.ssl3.rsa_fips_des_ede3_sha", false);

// ciphers with ECDH (without /e$/)
user_pref("security.ssl3.ecdh_rsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdh_rsa_aes_256_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_des_ede3_sha", false);
user_pref("security.ssl3.ecdh_ecdsa_aes_256_sha", false);

// 256 bits without PFS
user_pref("security.ssl3.rsa_camellia_256_sha", false);
user_pref("security.ssl3.rsa_aes_256_sha", false);

// ciphers with ECDHE and > 128bits
user_pref("security.ssl3.ecdhe_rsa_aes_256_sha", true);
user_pref("security.ssl3.ecdhe_ecdsa_aes_256_sha", true);

// GCM... yes please!
user_pref("security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256", true);
user_pref("security.ssl3.ecdhe_rsa_aes_128_gcm_sha256", true);

// susceptible to the logjam attack – https://weakdh.org/
user_pref("security.ssl3.dhe_rsa_camellia_256_sha", false);
user_pref("security.ssl3.dhe_rsa_aes_256_sha", false);

// ciphers with DSA (max 1024 bits)
user_pref("security.ssl3.dhe_dss_aes_128_sha", false);
user_pref("security.ssl3.dhe_dss_aes_256_sha", false);
user_pref("security.ssl3.dhe_dss_camellia_128_sha", false);
user_pref("security.ssl3.dhe_dss_camellia_256_sha", false);
user_pref("security.ssl3.dhe_dss_des_ede3_sha", false);

// fallbacks
user_pref("security.ssl3.rsa_aes_256_sha", true);
user_pref("security.ssl3.rsa_aes_128_sha", true);
